#Copyright 2018 Michal Kloko
#
#This file is part of Reaper MIDI Feedback.
#
#    Reaper MIDI Feedback is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    Reaper MIDI Feedback is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Reaper MIDI Feedback.  If not, see
#    <https://www.gnu.org/licenses/>.


# About:
# Assuming only one track is selected, moves selection to next track. If
# no track is selected, selects first track.

from reaper_python import *

selectedTrack = RPR_GetSelectedTrack(0,0)

nextTrack = False

for i in range( RPR_CountTracks(0) ):
    track = RPR_GetTrack(0, i)

    if selectedTrack == "(MediaTrack*)0x0000000000000000":
        RPR_SetTrackSelected(track, True)
        break
    elif track == selectedTrack:
        nextTrack = True # Flag so next iteration will be the actual
        # next track and we'll operate then...If we've already selected
        # the last track, the loop will not run again to act on this.
    elif nextTrack:
        RPR_SetTrackSelected(selectedTrack, False)
        RPR_SetTrackSelected(track, True)
        break
