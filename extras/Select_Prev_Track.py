#Copyright 2018 Michal Kloko
#
#This file is part of Reaper MIDI Feedback.
#
#    Reaper MIDI Feedback is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    Reaper MIDI Feedback is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Reaper MIDI Feedback.  If not, see
#    <https://www.gnu.org/licenses/>.


# About:
# Assuming only one track is selected, moves selection to previous
# track. If no track is selected, selects last track.

from reaper_python import *

selectedTrack = RPR_GetSelectedTrack(0,0)
trackCount = RPR_CountTracks(0)

if selectedTrack == "(MediaTrack*)0x0000000000000000":
    track = RPR_GetTrack(0, trackCount - 1)
    RPR_SetTrackSelected(track, True)
else:
    prevTrack = None

    for i in range(trackCount):
        track = RPR_GetTrack(0, i)

        if track == selectedTrack and prevTrack:
            RPR_SetTrackSelected(prevTrack, True)
            RPR_SetTrackSelected(track, False)
            break
        else:
            prevTrack = track
