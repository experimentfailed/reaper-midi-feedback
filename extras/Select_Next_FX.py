#Copyright 2018 Michal Kloko
#
#This file is part of Reaper MIDI Feedback.
#
#    Reaper MIDI Feedback is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    Reaper MIDI Feedback is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Reaper MIDI Feedback.  If not, see
#    <https://www.gnu.org/licenses/>.


# About:
# Assuming only one FX is selected, moves selection to next FX on
# selected track.

from reaper_python import *

track = RPR_GetSelectedTrack(0,0)
fxIndex = RPR_TrackFX_GetChainVisible(track)

if fxIndex < RPR_TrackFX_GetCount(track) - 1:
    RPR_TrackFX_SetOpen(track, fxIndex + 1, True)
