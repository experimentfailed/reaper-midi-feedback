#Copyright 2018 Michal Kloko
#
#This file is part of Reaper MIDI Feedback.
#
#    Reaper MIDI Feedback is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    Reaper MIDI Feedback is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Reaper MIDI Feedback.  If not, see
#    <https://www.gnu.org/licenses/>.

# About ################################################################
# Idk what's more confusing: the format of state chunds or why they are
# exposed without the devs giving us a format spec....I therefore
# assumed the structrue must be obvious (or would quickly become
# obvious), in my previous experiments. I was wrong.
#
# Now, I'm trying to spell it out in Python, according to what I have
# been able to find...So far, this approach has been the most
# successful.
#
# Anyway, the next step after this, is to break it down into discrete
# objects, if possible...
#
# Either way, lastly, will implement write support.
#
# oh, it also needs to support parameters that are used more than once
# in a section, which I thought of before, and think this solution
# supports it, but I don't remember the exact idea as I write this...'


# Script ###############################################################
import time
from collections import OrderedDict as dict

with open("../archive/example_track_chunk.txt") as f:
    chunk = f.readlines()


def devout(d):
    for k,v in d.items():
        print("%s: %s\n" % (k,v))


def chunkToDict(chunk):
    chunkDict = dict()
    currSection = chunkDict

    for l in chunk:
        l = l.split()
        lenl = len(l)

        fw = l[0]

        if fw.startswith('<'):
            fw = fw.lstrip('<')

            if fw == "TRACK":
                continue
            #~ elif fw == "RECCFG":
                #~ pass
            #~ elif fw == "AUXPANENV":
                #~ pass
            #~ elif fw == "AUXMUTEENV":
                #~ pass
            #~ elif fw == "VOLENV2":
                #~ pass
            elif fw == "FXCHAIN":
                if chunkDict:
                    currSection = chunkDict["FXCHAIN"] = dict()

                currSection["FX"] = []

            elif fw in ("VST", "JS"):
                if "FXCHAIN" in chunkDict:
                    currSection = dict()
                    fxList = chunkDict["FXCHAIN"]["FX"]

                elif "FX" in currSection:
                    fxList = currSection["FX"]

                currSection[
                    ' '.join(l[1:-4]).strip('""')
                ] = l[-4:-1]

                currSection["PARMLEARN"] = []

                fxList.append(currSection)

            elif fw == "ITEM":
                if chunkDict:
                    currSection = dict()
                    chunkDict["ITEMS"] = []

                    chunkDict["ITEMS"].append(currSection)

            #~ elif fw == "SOURCE":
                # SOURCE has to distinquish SECTION, WAVE, and MIDI.
                # WAVE can be in SECTION or in ITEM...fml...
                #~ pass

            else:
                #~ devout()
                #~ raise(Exception("Unknown section: %s" % fw))
                break

        elif fw == "PARMLEARN":
            currSection["PARMLEARN"].append(l[1:])

        elif fw == "PRESETNAME":
            currSection["PRESETNAME"] = ' '.join(l[1:]).strip('""')

        elif lenl > 2:
            currSection[fw] = str(l[1:])
        elif lenl == 2:
            currSection[fw] = str(l[1])

    return chunkDict


start = time.time()
chunkDict = chunkToDict(chunk)
duration = time.time() - start

devout(chunkDict["FXCHAIN"]["FX"][0])

print("\n\nDuration: %s\n" % duration)
