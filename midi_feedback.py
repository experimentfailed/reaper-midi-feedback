#Copyright 2018 Michal Kloko
#
#This file is part of Reaper MIDI Feedback.
#
#    Reaper MIDI Feedback is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    Reaper MIDI Feedback is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Reaper MIDI Feedback.  If not, see
#    <https://www.gnu.org/licenses/>.


# Config ###############################################################
# devices: To find my device number(s), in REAPER prefs under the MIDI
# Devices / Output section, I had to count the entries manually from 16
# (first output).
#
# A Windows user reported their offset was 17, so it may require some
# trial and error. On Linux, I recommend using a2jmidid.
#
# Once you know your device number(s), put them in the following list,
# according to which channel(s) they will use.
#
# Alternatively, use the very top slot for a single device using all
# channels. This will override any other entries.
#
# For example: My BCR2000 is 4th in MIDI Devices / Output list, so it's
# device number for me is 19. I have configured my BCR so there are CCs
# on channels 1, 2 and 3. Thus, I could put my device number in the
# marked slots according to channel in the following list. However,
# since I only have my BCR, I opted to put it in the very first slot and
# leave all else at 0.
#
# This allows multiple devices to be used at the same time with this
# script, provided you can configure your devices to use a single MIDI
# channel at a time OR you can somehow configure the channels they use
# around eachother.
#
# Make sure to enabled your outputs in REAPER!
devices = (
    19, # Set this for single device on all channels (overrides below).
    0,  # Device using MIDI channel 1,
    0,  # 2,
    0,  # 3,
    0,  # 4,
    0,  # 5,
    0,  # 6,
    0,  # 7,
    0,  # 8,
    0,  # 9,
    0,  # 10,
    0,  # 11,
    0,  # 12,
    0,  # 13,
    0,  # 14,
    0,  # 15,
    0   # 16
)

# bcr_lightsOUt: When enabled, with all of your BCR2000 encoders' LED
# display mode set to one of the "minus" settings (e.g. "1d-"; see page
# 16 and 17 in the BCR/BCF manual), this will translate mapped
# parameters of value 0 to 1, so the LED will illuminate at the minimum
# value, thus allowing us to turn them OFF when a CC is not mapped. This
# gives a much clearer picture of your mapping.
#
# This may work with other controllers that support similar LED
# settings.
#
# Set 1 to enable, 0 to disable.
bcr_lightsOut = 1


# Script ###############################################################

## DEAR SELF: Remember "To-Do" at the bottom of this scipt!

from reaper_python import *


def cmsg(*args, **kwargs):
    """Send console messages in a semi-clean way. Mix and match args &
    kwargs to get (keys/)values (converted to str) printed to REAPER
    console."""

    RPR_ShowConsoleMsg(
        "%s\n%s" % (
            ' '.join( str(a) for a in args ),
            '\n'.join( "%s=%s" % (k,str(v)) for k,v in kwargs.items() )
        )
    )


def reset():
    """Send value 0 to all devices' CCs"""

    # I feel like there's probably a faster way, but this isn't too bad,
    # performance-wise...
    for ch in range(1, 17):
        device = devices[0] or devices[ch]

        # If you wanted to do a program change, here's a start...The
        # BCR/BCF2000s do not support it, so I can't implement :(
        # RPR_StuffMIDIMessage(0, 0xb0 + ch, 0, msb)
        # RPR_StuffMIDIMessage(0, 0xb0 + ch, 0x20, lsb)
        # RPR_StuffMIDIMessage(0, 0xc0 + ch, program, 0)

        if not device: # Avoid sending to device 0...that could be bad.
            continue

        for cc in range(0, 128):
            RPR_StuffMIDIMessage(device, 0xB0 + ch, cc, 0)


lastFX = None
lastTrack = None

trackIndex = 0 # Used to use a for-loop in the main function. Now
# looping the function itself to see if it would reduce LED flickering
# on my BCR by reducing the number of MIDI messages sent, as it now only
# looks at one track vs all tracks per call. I think it helped slightly.


def syncCCs():
    """The main function of this ReaScript. Loops until terminated in
    REAPER. To terminate when running, re-run the script in REAPER."""

    global lastFX, lastTrack, trackIndex

    # I think some of the args in these two functions have to do with
    # indexing within a multi-selection...If so, I'll need rules for
    # that too. Nevertheless, 0 is getting me by for now.
    selectedTrack = RPR_GetSelectedTrack(0,0)
    selectedFX = RPR_GetFocusedFX(0, 0, 0)

    # Every time the focused FX or selected track changes, we want
    # to zero out any unused CCs to best reflect the new environment.
    # This is one of the reasons why motorized faders are not currently
    # not recommended with this script, at this time.
    if selectedFX != lastFX or lastTrack != selectedTrack:
        lastFX = selectedFX
        lastTrack = selectedTrack
        reset()

    track = RPR_GetTrack(0, trackIndex)

    selectedFXIndex = RPR_TrackFX_GetChainVisible(track) # So far,
    # this is the best way I can find to detect when no FX is selected,
    # so we can clear the controller. However, no way I can see to tell
    # when an FX window is closed...

    # See README.md & forum thread for more about track chunk size...
    # Sounds like limit is going away...Not sure how to handle that yet.
    trackChunk = RPR_GetTrackStateChunk(track, "", 4194304, False)[2]

    chainIndex = -1 # Keep track of the FX "index" we are on within the
    # chunk so we can know if the PARMLEARN we're parsing is on the
    # selected FX. Plus, we want to get the FX parameter from the FX
    # we're parsing in the chunk (if we even deicde we need it, below).

    for line in trackChunk.split('\n'):
        if not line:
            continue

        words = line.split()

        firstWord = words[0]

        if firstWord in ("<JS", "<VST"):
            chainIndex += 1
        elif firstWord == "PARMLEARN":
            m = int(words[3]) # The "Enable only when..." options in
            # param -> Learn

            # Update controller as needed
            # PARMLEARN options: 1 4 2 0; combinations are summed
            if (m in (0, 2)
                or m in (1, 3) and track == selectedTrack
                or m in (4, 6) and (
                    trackIndex + 1 == selectedFX[1]
                    and chainIndex == selectedFX[3]
                    and selectedFXIndex != -1
                )
            ):
                ccCh = int(words[2])
                ch, cc = ccCh & 0x0F, ccCh >> 8

                device = devices[0] or devices[ch]

                if not device:
                    continue

                # Get mapped param value from FX / convert to MIDI value
                # (0-127)
                p = RPR_TrackFX_GetParam(
                    track,
                    chainIndex,
                    int(words[1]), # Param index
                    0, # minvalOut??
                    0 # maxvalOut?!
                )

                fxMin, fxMax = p[4], p[5]
                val = int( (p[0] - fxMin) * 127 / (fxMax - fxMin) )

                RPR_StuffMIDIMessage(
                    device,
                    0xB0 + ch,
                    cc,
                    1 if bcr_lightsOut and val == 0 else val
                )

    if trackIndex >= RPR_GetNumTracks():
        trackIndex = 0
    else:
        trackIndex += 1

    RPR_defer("syncCCs()")


RPR_atexit("reset()")

syncCCs()


# To-Do (this is chaos right now):
# - Optional or enforced track chunk data buffering? I may also make a
#   track chunk class that can just tell me what I want on the fly,
#   which would go along well with caching, too.
#
# - Channel strip mode (use fxchain file created by user in REAPER, in
#   which they can add effects to define an "alternate" mapping for each
#   that will be used when no plugin is selected, but a track is - this
#   will require writing back to the track chunk to work).
#
# - Figure out motorized faders...At minimum, this requires being able
#   to only send MIDI messages to those CCs who's mapped parameter value
#   has changed. However, my attempts at implementing this seemingly
#   simple thing have failed. Should be able to make a global list of
#   all channels/CCs ( [[0] * 128] * 16 ) that you update (and send a
#   MIDI message) only when the new value != value in list.
#
# - Figure out if this script even makes sense outside of a BCR2000...
#   I only assume there are competing devices out there of some sort or
#   another. If it only makes sense for a BCR, I'll change the official
#   name to REAPER BCR2000 Feedback or some such.
#
# - Document personal use-case for further clarification (some of which
#   is already documented throughout the README and script comments)
#
# - Reset controller when nothing selected (sorta works now; see
#   comments above).
