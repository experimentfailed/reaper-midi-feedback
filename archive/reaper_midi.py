# I don't remember what this is...Pretty sure it's not actually for reaper
import pygame
import pygame.locals as pgl
import pygame.midi

midi_in = 3
 
pygame.init()
pygame.fastevent.init()
event_get = pygame.fastevent.get
event_post = pygame.fastevent.post
 
pygame.midi.init()
i = pygame.midi.Input(midi_in)
window = pygame.display.set_mode((468, 60))
 
mt = None
going = True
while going:
    events = event_get()
    for e in events:
        if e.type in [pgl.QUIT]:
            going = False
        if e.type in [pgl.KEYDOWN]:
            going = False
        if e.type in [pygame.midi.MIDIIN]:
            print(e), mt
 
    if i.poll():
        midi_events = i.read(10)
        mt = pygame.midi.time()
        # convert them into pygame events.
        midi_evs = pygame.midi.midis2events(midi_events, i.device_id)
 
        for m_e in midi_evs:
            event_post( m_e )
 
del i
pygame.midi.quit()

#~ from subprocess import Popen, PIPE

#~ MAX_HISTORY = 10

#~ def run(command):
    #~ process = Popen(command, stdout=PIPE, shell=True)
    #~ while True:
        #~ line = process.stdout.readline().rstrip()

        #~ if line:
            #~ yield line


#~ if __name__ == "__main__":
    #~ try:
        #~ history = ["Press ctrl+c to exit"]

        #~ for midiEvent in run('amidi --port="hw:3,0,0" --dump'):
            #~ if len(history) >= MAX_HISTORY:
                #~ history.pop(0)

            #~ if midiEvent:
                #~ history.append(midiEvent)

            #~ print('\n'.join(history), end = '', flush = True)
    #~ except KeyboardInterrupt:
        #~ print("\nExiting. Hope you had fun!")
