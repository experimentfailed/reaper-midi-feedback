# This file mainly contains random ideas, I was exploring before I
# figrued out the first release versions
from reaper_python import *
import os, time, random

# Config
fxchainFile = "/home/statik/.config/REAPER/FXChains/test1.RfxChain"

# Startup

## Load FX map from fxchain

fxchainMap = []

# Note: PARAMLEARN mode (last number; from fxchain save file)
# 1 4 2 0 - combinations are summed

# From fx chain file, read/map each FX's PARAMLEARN CC's (channel not
# supported...  ).
with open(fxchainFile) as f:
    for line in f.readlines():
        if line.startswith('<'):
            fxchainMap.append({})
        else:
            words = line.split()

            if words[0] == "PARMLEARN":
                fxchainMap[-1][int(words[1])] = int(words[2]) >> 8

RPR_ShowConsoleMsg(str(fxchainMap)+'\n')

# Notes/tests
selectedTrack = RPR_GetSelectedTrack(0,0) # Not sure what the args do
selectedFXIndex = RPR_TrackFX_GetChainVisible(selectedTrack)

test = RPR_GetTrackStateChunk(selectedTrack, "", 1024, False)[2]

#~ test = RPR_TrackFX_GetParam(
    #~ selectedTrack,
    #~ selectedFX, # FX index
    #~ 0, # Param index
    #~ 0, # ? minvalOut
    #~ 0 # ? maxvalOut
#~ )

#~ test = fxchainMap[selectedFXIndex]

#~ RPR_ShowConsoleMsg( str(paramCnt)+'\n' )
RPR_ShowConsoleMsg( str(test)+'\n' )

#~ def main():
    # Read selected fx
    # update controller
    #~ RPR_defer("main()")

#~ main()


# Some old stuff & tests ###############################################
#~ selectedFXName = RPR_TrackFX_GetFXName(
    #~ selectedTrack, selectedFXIndex, "", 32
#~ )[3]

#~ fxCnt = RPR_TrackFX_GetCount(selectedTrack)

#~ bcrP1CCs = (
    #~ (33,40), # Top row push
    #~ (1,8), # Top row twist
    #~ (65,72), # Button row 1
    #~ (73,80), # Button row 2
    #~ (81,88),
    #~ (89,96),
    #~ (97,104),
    #~ (105,108) # Bottom-right 4 buttons
#~ )

#~ allEncoders = {cc:0 # cc:val
    #~ for row in bcrP1CCs for cc in xrange(row[0], row[1]+1)
#~ }

#~ def syncEncoders():
    #~ """Update all physical encoders at once with values contained in
    #~ allEncoders."""

    #~ os.system(
        #~ '~/sendmidi-linux-1.0.13/sendmidi '
        #~ 'dev "BCR2000 MIDI 1" %s' % ' '.join(
            #~ "cc %s %s" % (cc, val)
            #~ for cc, val in allEncoders.items()
        #~ )
    #~ )


#~ def setEncoder(cc, val):
    #~ """Set an encoder's value. For bulk updates, it should be faster to
    #~ set the values in the allEncoders dict, then call syncEncoders()"""

    #~ os.system(
        #~ '~/sendmidi-linux-1.0.13/sendmidi '
        #~ 'dev "BCR2000 MIDI 1" ch 0 cc %s %s' % (cc, val)
    #~ )

    #~ allEncoders[cc] = val

    #~ # Need to figure out these:
    #~ ## numMIDIOutputs = RPR_GetNumMIDIOutputs()
    #~ ## test = RPR_GetMIDIOutputName(dev, nameout, nameout_sz)
    #~ ## RPR_StuffMIDIMessage(Int mode, Int msg1, Int msg2, Int msg3)
    #~ # Maybe a way to get selected FX if needed:
    #~ ## TrackFX_GetChainVisible(MediaTrack* track) # returns index of
    #~ # effect visible in chain, or -1 for chain hidden, or -2 for chain
    #~ # visible but no effect selected


#~ def reset():
    #~ """Set all encoders to 0"""

    #~ for cc in allEncoders:
        #~ allEncoders[cc] = 0

    #~ syncEncoders()


#~ def test():
    #~ """Sets all encoders to 0, then random, then 0 again."""

    #~ reset()

    #~ for cc in allEncoders:
        #~ allEncoders[cc] = random.randint(0,127)

    #~ syncEncoders()

    #~ time.sleep(1)

    #~ reset()


#~ test()

#~ lastTrack = None
#~ lastFX = None

def test():
    global lastFX

    #~ for track in xrange( RPR_GetNumTracks() ):
    # RPR_GetFocusedFX(tracknumberOut, itemnumberOut, fxnumberOut)
    # Once again, I don't understand the args fully... Why "Out"?
    # fxnumberOut!? PC Load Letter!? WTF DOES THAT MEAN!?
    #
    # Calling as follows, return goes:
    # [0] = 1 if a track FX window has focus, 2 if an item FX window
    # has focus, 0 if no FX window has focus.
    # [1] = Track index (0 = master)
    # [2] = Item index
    # [3] = FX index
    selectedTrack = RPR_GetSelectedTrack(0,0)

    # RPR_GetFocusedFX(tracknumberOut, itemnumberOut, fxnumberOut)
    selectedFX = RPR_GetFocusedFX(
        int(
            RPR_GetMediaTrackInfo_Value(
                selectedTrack,
                "IP_TRACKNUMBER"
            )
        ),
        0,
        0
    )

    if selectedFX != lastFX:
        lastFX = selectedFX

        cmsg(lastFX, selectedFX)

    RPR_defer("test()")


#~ test()
