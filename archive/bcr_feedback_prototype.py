#Copyright 2018 Michal Kloko
#
#This file is part of Reaper MIDI Feedback.
#
#    Reaper MIDI Feedback is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    Reaper MIDI Feedback is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Reaper MIDI Feedback.  If not, see
#    <https://www.gnu.org/licenses/>.


# About ################################################################
# Here is the original working prototype before I figured out how to
# use RPR_StuffMIDIMessage(). It also doesn't handle JS plugins.

from reaper_python import *
import os

selectedTrack = RPR_GetSelectedTrack(0,0) # Not sure what the args do
selectedFXIndex = RPR_TrackFX_GetChainVisible(selectedTrack)

# No clue how to get enough data for sure...
chunk = RPR_GetTrackStateChunk(selectedTrack, "", 1024, False)[2]


def getCCValuesFromSelectedFX():
    for i,v in enumerate(chunk.split('<VST')[1:]):
        if i == selectedFXIndex:
            for line in v.split('\n'):
                words = line.split()

                if words and words[0] == "PARMLEARN":
                    # >> 8 converts to hardware encoder CC

                    # How to get channel? The solution in the source for
                    # the above did not work...Source needed...

                    # Get/convert param value from FX
                    p = RPR_TrackFX_GetParam(
                        selectedTrack,
                        i, # FX index
                        int(words[1]), # Param index
                        0, # ? minvalOut
                        0 # ? maxvalOut
                    )

                    fxMin = p[4]
                    fxMax = p[5]

                    # Yeild (cc, value)
                    yield (
                        int(words[2]) >> 8,
                        int( (p[0] - fxMin) * 127 / (fxMax - fxMin) )
                    )


values = [0] * 128

def syncEncoders():
    """Update all physical encoders at once"""

    for cc, val in getCCValuesFromSelectedFX():
        values[cc] = val

    os.system(
        '~/sendmidi-linux-1.0.13/sendmidi '
        'dev "BCR2000 MIDI 1" %s' % ' '.join(
            "cc %s %s" % (cc, val) for cc, val in enumerate(values)
        )
    )

    RPR_defer("syncEncoders()")

syncEncoders()
