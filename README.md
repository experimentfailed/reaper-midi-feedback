# Reaper MIDI Feedback

## License

Copyright 2018 Michal Kloko, All Rights Reserved

This file is part of Reaper MIDI Feedback.

    Reaper MIDI Feedback is free software: you can redistribute it
    and/or modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    Reaper MIDI Feedback is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Reaper MIDI Feedback.  If not, see
    <https://www.gnu.org/licenses/>.


## About

DO NOT USE WITH MOTORIZED FADERS (SEE DEV NOTES BELOW)!!!

This script essentially looks at your project's FX MIDI mapping, then,
depending on selected track and/or FX (based on "Enable only when..."
settings in param -> Learn), sends MIDI messages back to the controller
so it can update it's lights/faders/etc. to reflect each mapped
parameter's value on the physical device.

Another way of looking at it, is that it eliminates the need for soft
takeover on endless encoders, so it may even be of some use to devices
without LED indicators, too, but I can't be sure.

Status: alpha

 - Testing has been extremely limited but so far so good!

 - It's only been tested with a Behringer BCR2000 controller in Absolute
 and Relative modes (not 14-bit). I also have set my encoders to turn
 off the LED when at zero (1d-). See bcr_lightsOut in the Config
 section.

 - Supports multiple controllers, but testing has been minimal

 - Performance has not been tested enough. Projects with a lot of FX
 will be slower (again, see below).

 - Some FX may not get picked up by the script, due to track chunk
 truncation (say that 5 times fast). It seems, however, Cockos is
 removing the limit. See link by nofish on the first page of the forum
 thread.

 - It does not seem to properly respect a no-selection scenerio (as in:
 no plugin selected -> controller state does not clear). I am
 investigating this, but it's not a huge deal.

 - It was written in Linux, for Linux and I have no direct way to
 support other platforms. However, it should still work (see below), and
 I'll do my best to help if it doesn't!

 - I am new to both ReaScript and programming for MIDI devices - this is
 my first attempt at both.

 - It should be compatible with Python 2.7+ and 3.*, but at this moment,
 it's being developed in 2.7, as I have yet to get a version of 3
 working with REAPER.

 - Motorized faders have not been tested - use with caution and use in
 absolute mode only (see Dev Notes below). I would generally avoid it
 for now, though, and I may end up not being able to support them really
 at all.

Please also refer to the [Cockos Forum thread.](https://forums.cockos.com/showthread.php?t=209353)


## Usage

The script ususally comes in 2 versions - experimental (usually bleeding
edge and may sometimes flat out not work at all), and "latest", aka
midi_feedback.py (which will generally be the most tested). This is
mainly for my own benefit, so I can easily go back a few steps if my
experiments fail. I'll usually update the above "Status" (in this file)
according to how I feel the "latest" to be fairing (eventually, it
should always just be "stable").

Other scipts are included, under the extras directory, that may or may
not ever be useful to others. The 4 in there right now are covered by
SWS/S&M, as far as I know. I just haven't gotten around to building it
for my system yet. See Dev Notes below before using.

Also be aware that, at this time (and possibly forever), I can't seem to
find a way to detect when all plugin windows are closed, so we can clear
any "Enable only when effect configuration is focused" mappings. As it
is, after you close the last FX window, change the track selection and
the controller will update. Otherwise, so far, it seems to keep up fine.


### Installation:

 1. Clone the repo, for example (roughly how I do it):

        cd ~/my_git_repos/
        git clone https://experimentfailed@bitbucket.org/experimentfailed/reaper-midi-feedback.git

    Then updates can be obtained like:

        cd ~/my_git_repos/reaper-midi-feedback
        git pull

 2. Copy (or better yet symlink for easier updates) your desired script
 version(s) into your preferred REAPER scripts directory (default is
 ~/.config/REAPER/Scripts on Linux).

You could alternatively copy and paste the code from the repo into a new
.py file of your own making, if you can't be bothred with git.

### Configuration

#### Script

 1. Edit the script. After the license text, you'll see the "# Config"
 section.

 2. Change the "devices" setting according to the instructions provided
 in the script file.

 3. Check out any other options and set as desired.

#### REAPER

 1. Enable the device for input (control only is recommended) and output
 in REAPER (Options -> Preferences -> MIDI devices).

 2. Add the script in REAPER (from main menu click Actions ->
 Show Action List, look toward the bottom right of the window that
 appears for ReaScript...click Load and browse to your desired
 version(s) of this script).

 3. Run the script from the Actions list (same window as above). Double
 clicking on it should do the trick.

 4. To stop the script, run the action again and click "Terminate
 Instance" on the dialog that pops up.

Now map your MIDI device as you normally would in REAPER. It is advised
to save your mappings using fxchain files so you can reload them in all
of your projects.


## Dev Notes

Performance could get hairy with huge projects and lots of plugins, due
to parsing the track chunks on every RPR_Defer(). I have a couple of
ideas, should it become an issue. Not the least of which, would be to
cache the chunks, and update them less frequently or require a script
restart.

As for other platforms, it should still work, no? Just need to add other
plugin types to that tuple: ("<JS", "<VST"), I think...Someone will have
to look at the track chunks to see if there are any other differences.

LED flickering is also an issue on my BCR. It seems to be due to the
frequency of updates. I would rather only update when a parameter value
has actually changed since last check, but my best attempts so far fall
short (mostly failing completely) and I'm not yet sure why. It's pretty
subtle, too, so most people probably wont even notice....UPDATE: I've
found a couple of ways to reduce updates now; the problem seems mostly
gone, but does not solve motorized faders (below).

It seems parameter movements are a bit jumpy for me...not sure why.
Relative modes seem smooth, and much more precicse overall, but requires
a lot more twisting. I have only briefly tried 14-bit and it didn't
instantly work w/ REAPER, so will have to look further into that

On motorized faders: I realized after a thought experiment, that the
frequency of MIDI messages this script is sending, could lead to early
servo burnout, aka hardware failure (depending on how the controller is
designed). As in, if the controller tries to adjust the fader regardless
of whether or not the value actually changed, I don't know what that
could do to the servo, if anything. The effect would, of course, be
amplified on non-touch sensitive faders, when you move them. The script
will essentially always be trying to pull the fader back a little b/c
it's always going to be a tad behind. Further more, the way I'm updating
the controllers now, sends 0 to all CCs every time track or plugin
selection changes (almost), which is going to add even more wear to any
kind of fader. If I can get my fix for the aforementioned flickering
LEDs to work, that should largely solve this too.

Extra scripts: They seem to work unless I enable "Open track FX window
on track selection change" in REAPER prefs, while using mapped buttons
on my BCR or keyboard shortcuts. In that case, whenever I put some FX on
a track, push the mapped button/keys for Select_<Next/Prev>_FX.py,
then push the mappped button/keys for Select_<Next/Prev>_Track.py, I get
a Segmentation Fault in REAPER (and hard crash/exit). This, however,
does NOT happen when I run the cripts manually by double-clicking in the
Actions window.

See also To-Do list at bottom of script variations.


## Additional resources/credit:

I don't think I used any code from any of these links (other than some
binary operations for MIDI messages), but either way did use them
heavily for example/reference:

[https://forum.cockos.com/showthread.php?p=2015291#post2015291]

[https://github.com/MichaelPilyavskiy/ReaScripts/blob/master/FX/mpl_Save%20all%20track%20FX%20chains.lua]

[https://forums.cockos.com/showthread.php?p=1949134]

[https://github.com/jtackaberry/reaticulate]

[https://ccrma.stanford.edu/~craig/articles/linuxmidi/misc/essenmidi.html]

[https://github.com/jtackaberry/reascripts/blob/master/MIDI/tack_Functions.lua]

[http://forum.cockos.com/showthread.php?t=168383]

[https://forums.cockos.com/showthread.php?p=1638594]

[https://github.com/X-Raym/REAPER-ReaScripts/tree/master/MIDI%20Editor]
